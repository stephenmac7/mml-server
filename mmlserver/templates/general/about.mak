<%inherit file="base.mak"/>
<h2>${title}</h2>
<hr>
<h4>Permissions</h4>
When assesing the legality of having a copyrighted mod on this website, we assume:
<ol>
    <li>If no permission is specified for this unique situation, one must follow restrictions given for 'modpacks'.</li>
    <li>No license given (or none which applies) by the creator means all rights reserved.</li>
</ol>